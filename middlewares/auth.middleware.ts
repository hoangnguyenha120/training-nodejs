const jwt = require('jsonwebtoken');

const authenticationMiddleware = () => {
  return (req, res, next) => {
    try {
      const token = req.header('Authorization');

      req.body.user = jwt.verify(token, process.env.JWT_PRIVATE_KEY);

      return next();
    } catch (error) {
      console.log(error);
      return res.send('Un-Authentication');
    }
  };
};

export {
  authenticationMiddleware,
};
