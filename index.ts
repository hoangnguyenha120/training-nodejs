import express from 'express';
import dotEnv from 'dotenv';
import bodyParser from 'body-parser';
import helmet from 'helmet';

import { AuthController } from './controllers/auth.controller';
import { UserController } from './controllers/user.controller';

dotEnv.config();

const app = express();

app.use(helmet());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// Logger middleware
app.use('/', (req, res, next) => {
  const now = new Date();
  console.log(now.toDateString(), now.toLocaleTimeString(), req.path);

  return next();
});

const API_PORT: number = +(process.env.API_PORT || 3000);

app.get('/', (req, res, next) => {
  res.send('Hello world222');
});

app.use('/auth', AuthController);
app.use('/user', UserController);

app.listen(API_PORT, () => {
  console.log('Server is running on port:', API_PORT);
});
