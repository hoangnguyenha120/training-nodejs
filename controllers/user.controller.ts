import express from 'express';
import { authenticationMiddleware } from '../middlewares/auth.middleware';

const UserController = express.Router();

UserController.get('/info',
  authenticationMiddleware(),
  (req, res, next) => {
    const { user } = req.body;
    console.log('user', user);

    return res.send('Information controller');
  });

export {
  UserController,
};
