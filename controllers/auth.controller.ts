import express from 'express';

const jwt = require('jsonwebtoken');
import bcrypt from 'bcrypt';

const AuthController = express.Router();

const FAKE_USERNAME: string = 'hoangnt';
const FAKE_PASSWORD: string = '123456';

/**
 * @description Login api
 */
AuthController.post('/login', async (req, res, next) => {
  const PRIVATE_KEY: string = process.env.JWT_PRIVATE_KEY!;
  // Generate salt
  const salt: string = bcrypt.genSaltSync(10);
  // Hash password
  const hash: string = bcrypt.hashSync(FAKE_PASSWORD, salt);

  const { username, password } = req.body;

  // Check username
  if (username !== FAKE_USERNAME) {
    return res.send('User not found');
  }

  // Check password
  const isPwsMatching: boolean = bcrypt.compareSync(password as string, hash);
  if (!isPwsMatching) {
    return res.send('Wrong password');
  }

  // Generate token
  const token: string = await jwt.sign({ username: FAKE_USERNAME }, PRIVATE_KEY);

  res.send(token);
});

const checkValidToken = (token: string) => {
  try {
    const PRIVATE_KEY: string = process.env.JWT_PRIVATE_KEY!;

    return jwt.verify(token, PRIVATE_KEY);
  } catch (error) {
    return false;
  }
};

/**
 * @description Get infor
 */
AuthController.get('/infor/:token', (req, res, next) => {
  const { token } = req.params;

  // Check is valid token
  const isValidToken = checkValidToken(token);

  res.send(isValidToken ? `Hello ${FAKE_USERNAME}` : 'No authen');
});

export {
  AuthController,
};
